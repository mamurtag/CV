/**

 * 
 */
package cs.tcd.ie;


import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.lang.Object;
import java.util.Timer;
import java.util.TimerTask;

import tcdIO.*;

/**
 *
 * Client class
 * 
 * An instance accepts user input 
 *
 */
public class Client extends Node {
	static final int DEFAULT_SRC_PORT = 50000;
	static final int DEFAULT_DST_PORT = 50001;
	static final String DEFAULT_DST_NODE = "localhost";	
	char ack='0';
	boolean packetSent;
	int i=0;

	Terminal terminal;
	InetSocketAddress dstAddress;

	/**
	 * Constructor
	 * 	 
	 * Attempts to create socket at given port and create an InetSocketAddress for the destinations
	 */
	Client(Terminal terminal, String dstHost, int dstPort, int srcPort) {
		try {
			this.terminal= terminal;
			dstAddress= new InetSocketAddress(dstHost, dstPort);
			socket= new DatagramSocket(srcPort);
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}


	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public synchronized void onReceipt(DatagramPacket packet) {
		StringContent content= new StringContent(packet);

		//		if(content.equals(ack))
		//		{
		//			if(i==0)
		//			{
		//
		//			}
		//			else
		//			{
		//				i--;
		//			}
		//		}
		terminal.println(content.toString());
		packetSent=true;
		this.notify();
	}


	/**
	 * Sender Method
	 * 
	 */
	public synchronized void start() throws Exception {
		byte[] data= null;

		DatagramPacket packet= null;

		String s = terminal.readString("String to send:" );
		s.trim();

		for(i=0;i<s.length();i++)
		{
			packetSent=false;
			if(ack=='0')
			{
				ack='1';
			}
			else if(ack=='1')
			{
				ack='0';
			}
			char c = s.charAt(i);
			data = new byte[2];
			data[0] = (byte) c;
			data[1]= (byte) ack;
			packet= new DatagramPacket(data, data.length, dstAddress);
			terminal.println("" + c );

			while(!packetSent)
			{
				socket.send(packet);
				this.wait(500);
			}
		}
		


		//			data= (terminal.readString("String to send: ")).getBytes();
		//			
		//			terminal.println("Sending packet...");
		//			packet= new DatagramPacket(data, data.length, dstAddress);
		//			socket.send(packet);
		//			terminal.println("Packet sent");
		//			this.wait();
	}


	/**
	 * Test method
	 * 
	 * Sends a packet to a given address
	 */
	public static void main(String[] args) {
		try {					
			Terminal terminal= new Terminal("Client");		
			(new Client(terminal, DEFAULT_DST_NODE, DEFAULT_DST_PORT, DEFAULT_SRC_PORT)).start();
			terminal.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}









