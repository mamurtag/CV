package cs.tcd.ie;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import tcdIO.Terminal;

public class Server extends Node {
	static final int DEFAULT_PORT = 50001;
	String temp="8";
	Terminal terminal;

	/*
	 * 
	 */
	Server(Terminal terminal, int port) {
		try {
			this.terminal= terminal;
			socket= new DatagramSocket(port);
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}

	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public synchronized void onReceipt(DatagramPacket packet) {
		try {
			StringContent content= new StringContent(packet);
			byte [] dataAck =packet.getData();
			String char1 = "" + (char) dataAck[0];
			String ack = "" + (char) dataAck[1];
			
			

			if(!temp.equals(ack))
			{
				temp=ack;
			}
			
			terminal.println(char1 + "\n");
			terminal.println("ACK: " +ack + "\n");
			
			DatagramPacket response;
			response= (new StringContent(temp)).toDatagramPacket();
			response.setSocketAddress(packet.getSocketAddress());


			socket.send(response);
			this.notify();



		}
		catch(Exception e) {e.printStackTrace();}
	}


	public synchronized void start() throws Exception {
		terminal.println("Waiting for contact");
		this.wait();
	}

	/*
	 * 
	 */
	public static void main(String[] args) {
		try {					
			Terminal terminal= new Terminal("Server");
			(new Server(terminal, DEFAULT_PORT)).start();
			//terminal.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}
