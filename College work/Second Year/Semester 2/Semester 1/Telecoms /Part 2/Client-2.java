/**
 * 
 */
package cs.tcd.ie;


import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import tcdIO.*;

/**
 *
 * Client class
 * 
 * An instance accepts user input 
 *
 */
public class Client extends Node {
	static final int DEFAULT_SRC_PORT = 50000;
	static final int DEFAULT_DST_PORT = 50001;
	static final String DEFAULT_DST_NODE = "localhost";	
	int ack=0;
	boolean packetSent;
	int i=0;
	int windowCount=2;
	int windowI=0;
	ArrayList<Integer> sent = new ArrayList<Integer>();
	ArrayList<Integer> received = new ArrayList<Integer>();
	Terminal terminal;
	InetSocketAddress dstAddress;

	/**
	 * Constructor
	 * 	 
	 * Attempts to create socket at given port and create an InetSocketAddress for the destinations
	 */
	Client(Terminal terminal, String dstHost, int dstPort, int srcPort) {
		try {
			this.terminal= terminal;
			dstAddress= new InetSocketAddress(dstHost, dstPort);
			socket= new DatagramSocket(srcPort);
			listener.go();
		}
		catch(java.lang.Exception e) {e.printStackTrace();}
	}


	/**
	 * Assume that incoming packets contain a String and print the string.
	 */
	public synchronized void  onReceipt(DatagramPacket packet) {
		StringContent content= new StringContent(packet);
		terminal.println(content.toString());
		byte [] dataAck =packet.getData();
		int r= (int) dataAck[0];
		received.add(r);
		
		terminal.println("" + r);
		System.out.println(r);
		
		//packetSent=true;
		this.notify();
			
	}


	/**
	 * Sender Method
	 * 
	 */
	public synchronized void start() throws Exception {
		byte[] data= null;
		DatagramPacket packet= null;


		Terminal terminal= new Terminal("Client");

		String s = terminal.readString("String to send:" );
		s.trim();
		

		
		for(int i=0; i<s.length(); i++)
		{
			
			if(windowI==windowCount)
			{
				
				for(int a=0; a<received.size();a++)
				{
					windowI=0;
					if(sent.get(a)!=received.get(a))
					{
						i=a;
						sent.clear();
						received.clear();
						break;
					}
				}
			}
			ack++;
			char c = s.charAt(i);
			
			data = new byte[2];
			data[0] = (byte) c;
			data[1]= (byte) ack;
			packet= new DatagramPacket(data, data.length, dstAddress);
			terminal.println("" + c );
			sent.add(ack);
			socket.send(packet);
			windowI++;
			
			this.wait(2000);

		}


//		data= (terminal.readString("String to send: ")).getBytes();
//
//		terminal.println("Sending packet...");
//		packet= new DatagramPacket(data, data.length, dstAddress);
//		socket.send(packet);
//		terminal.println("Packet sent");
//		this.wait();
	}


	/**
	 * Test method
	 * 
	 * Sends a packet to a given address
	 */
	public static void main(String[] args) {
		try {					
			Terminal terminal= new Terminal("Client");		
			(new Client(terminal, DEFAULT_DST_NODE, DEFAULT_DST_PORT, DEFAULT_SRC_PORT)).start();
			terminal.println("Program completed");
		} catch(java.lang.Exception e) {e.printStackTrace();}
	}
}
