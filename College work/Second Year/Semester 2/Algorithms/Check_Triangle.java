
public class Check_Triangle
{


	public static final int ACUTE_TRI = 2;
	public static final int RIGHT_ANGLED_TRI = 1;
	public static final int OBTUSE_TRI = 3;
	public static final double TINY_DIFF = 0.0001;
	public static final int NOT_TRI = 0;

	static boolean form_triangle(double[] ls)
	{
		double a=ls[0];
		double b =ls[1];
		double c=ls[2];

		if(2*max(ls)< a+b+c)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	static int kind_triangle(double[] ls)
	{
		double a= ls[0];
		double b=ls[1];
		double c=ls[2];

		if(form_triangle(ls)==false)
		{
			return NOT_TRI;
		}
		else if(((c*c) - ((a*a) + (b*b)))==0)
		{
			return RIGHT_ANGLED_TRI; 
		}
		else if(((c*c) - ((a*a) + (b*b)))<0)
		{
			return ACUTE_TRI;
		}
		else if(((c*c) - ((a*a) + (b*b)))>0)
		{
			return OBTUSE_TRI;
		}
		 
		return NOT_TRI;
	}


	static double sum(double[] arr)
	{
		double result = 0;
		for (int k = 0; k < arr.length; k++)
			result += arr[k];
		return result;
	}

	static double max(double[] arr)
	{
		int j = 0;
		int k = arr.length- 1;
		while ( j < k ) {
			if (arr[j] < arr[k])
				j++;
			else
				k--;
		}
		return arr[j];
	}

	static double min(double[] arr)
	{
		int j = 0;
		int k = arr.length - 1;
		while ( j < k )
			if (arr[j] > arr[k])
				j++;
			else
				k--;
		return arr[j];
	}
}
