#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "time.h"

#define NUM_THREADS 5
pthread_t phils[NUM_THREADS]; 
pthread_mutex_t forks[NUM_THREADS];



void *PrintHello(void *threadid)
{ 
	 printf("\n%d: Hello World!\n", threadid); 
	 pthread_exit(NULL);
}
void pickUpFork(int phil, int c, char * hand)
{
	pthread_mutex_lock (&forks[c]);
	printf ("Philosopher %d: got %s fork %d\n", phil, hand, c);
}

void putDownFork(int c1, int c2)
{
	pthread_mutex_unlock (&forks[c1]);
	pthread_mutex_unlock (&forks[c2]);
}

void * philosophere(void * num)
{
	time_t start = time(NULL);
	while(1)
	{
		
	
		long wait = 50000000;
		long wait2 = 500000000;
    
   
   	
   	 	int id, i, leftFork, rightFork, f;
		id = (int)num;
    
    	
   	 	printf("Philosopher %d is thinking.\n", id);
		while(wait!=0)
    	{
     	   wait--;
   		}
		usleep(5000000);
    	
		start = time(NULL);
		printf ("Philosopher %d is done thinking and now ready to eat.\n", id);
		rightFork= id;
		leftFork = id + 1;
	
		if (leftFork==NUM_THREADS)
		{
			leftFork= 0;
		}
	  		
		pickUpFork(id, rightFork, "right");
   	 	pickUpFork(id, leftFork, "left");
   	 	
		time_t new =time(NULL);
		printf ("Philosopher %d: eating.\n", id);
   		while(wait2!=0)
    	{
     	   wait2--;
   	 	}
   		time_t dif = new - start;
		printf ("Philosopher %d: waited %ld.\n", id, dif);
    
    	putDownFork(leftFork, rightFork);
		printf ("Philosopher %d is done eating.\n", id);
  	    
    }
    
}
	
int main (int argc, const char * argv[]) 
{
	int rc,t, mt;
	
	for(int i=0; i<NUM_THREADS; i++)
	{
		mt =pthread_mutex_init(&forks[i], NULL);
	}
	for (t=0; t<NUM_THREADS;t++)
	{
		
		rc = pthread_create(&phils[t], NULL, philosophere, (void *)t);
		if (rc) 
		{
			printf("ERROR return code from pthread_create(): %d\n",rc); exit(-1);
		}
		
  	}
	printf("  \n");
	
	// wait for threads to exit
	for(t=0;t<NUM_THREADS;t++)
	{
		pthread_join( phils[t], NULL);
	}
	
	return(0);
}
	  
	 
	  
	  
 








